import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { UrlErrorComponent } from './body/url-error/url-error.component';
import { AboutComponent } from './body/about/about.component';
import { BlogComponent } from './body/blog/blog.component';
import { BlogDetailsComponent } from './body/blog-details/blog-details.component';
import { LoginComponent } from './body/login/login.component';
import { PricingComponent } from './body/pricing/pricing.component';
import { HomeComponent } from './body/home/home.component';
import { ContactComponent } from './body/contact/contact.component';
import { RegisterComponent } from './body/register/register.component';
import { ForgotComponent } from './body/forgot/forgot.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    UrlErrorComponent,
    AboutComponent,
    BlogComponent,
    BlogDetailsComponent,
    LoginComponent,
    PricingComponent,
    HomeComponent,
    ContactComponent,
    RegisterComponent,
    ForgotComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
