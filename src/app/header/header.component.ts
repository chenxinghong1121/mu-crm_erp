import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  // 導航欄
  navbarClicked() {
    let navbarToggler = document.querySelector(".navbar-toggler");
    const navbarCollapse = document.querySelector(".navbar-collapse");
    navbarToggler?.classList.toggle("active");
    navbarCollapse?.classList.toggle("show");
  }

  // 子目錄
  submenuClicked() {
    const submenuToggler = document.querySelector(".ud-submenu")
    submenuToggler?.classList.toggle("show");
  }

  // 返回頁面頂部
  backTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    })
  }

}
