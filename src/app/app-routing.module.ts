import { ForgotComponent } from './body/forgot/forgot.component';
import { RegisterComponent } from './body/register/register.component';
import { HomeComponent } from './body/home/home.component';
import { UrlErrorComponent } from './body/url-error/url-error.component';
import { LoginComponent } from './body/login/login.component';
import { PricingComponent } from './body/pricing/pricing.component';
import { ContactComponent } from './body/contact/contact.component';
import { BlogDetailsComponent } from './body/blog-details/blog-details.component';
import { BlogComponent } from './body/blog/blog.component';
import { AboutComponent } from './body/about/about.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    title: 'Mu | 首頁',
    component: HomeComponent
  },
  {
    path: 'about',
    title: 'Mu | 關於',
    component: AboutComponent
  },
  {
    path: 'blog',
    title: 'Mu | 部落格',
    component: BlogComponent
  },
  {
    path: 'blog-details',
    title: 'Mu | 部落格',
    component: BlogDetailsComponent
  },
  {
    path: 'contact',
    title: 'Mu | 聯絡',
    component: ContactComponent
  },
  {
    path: 'pricing',
    title: 'Mu | 價格',
    component: PricingComponent
  },
  {
    path: 'login',
    title: 'Mu | 登入',
    component: LoginComponent
  },
  {
    path: 'register',
    title: 'Mu | 註冊',
    component: RegisterComponent
  },
  {
    path: 'forgot',
    title: 'Mu | 註冊',
    component: ForgotComponent
  },
  {
    path: '**',
    title: 'Mu | 404',
    component: UrlErrorComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top',
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
