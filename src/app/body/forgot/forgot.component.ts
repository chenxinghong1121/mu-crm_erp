import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {

  'forgotForm': FormGroup
  forgotError = false

  constructor(
    private formbuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.createforgotForm()
  }

  createforgotForm() {
    this.forgotForm = this.formbuilder.group({
      username: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern('^[A-Za-z0-9]+$'),
      ]
    ]
    })
  }

  forgotPassword() {
    
  }

}
