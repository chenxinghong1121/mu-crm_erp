import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  'loginForm': FormGroup;
  loginError = false

  constructor(
    private formbuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createLoginForm()
  }

  createLoginForm() {
    this.loginForm = this.formbuilder.group({
      username: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern('^[A-Za-z0-9]+$'),
      ]
    ],
      password: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern('^[A-Za-z0-9]+$')
      ]
    ]
    })
  }

  login() {
    const { username, password } = this.loginForm.value;

    if (username == 'username' || password == 'password') {
      this.router.navigate([''])
    }
    else {
      this.loginError = true
    }
  }

}
