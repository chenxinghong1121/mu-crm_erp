import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  'registerForm': FormGroup;

  constructor(
    private formbuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createRegisterForm()
  }

  createRegisterForm() {
    this.registerForm = this.formbuilder.group({
      nickname: ['', [
        Validators.required,
        Validators.maxLength(16),
        Validators.pattern('^[A-Za-z0-9]+$')
      ]
    ],
      username: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern('^[A-Za-z0-9]+$'),
      ]
    ],
      password: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern('^[A-Za-z0-9]+$'),
      ]
    ],
      email: ['', [
        Validators.required,
        Validators.email
      ]
    ]
    })
  }

  errorMessage(key: string): string {
    const formControl = this.registerForm.get(key)

    let errorMessage: string

    if (!formControl || !formControl.errors || formControl.pristine) {
      errorMessage = ''
    } 
    else if (formControl.errors['required']) {
      errorMessage = '此欄位為必填'
    } 
    else if (formControl.errors['minlength']) {
      errorMessage = '請輸入大於 8 個字'
    } 
    else if (formControl.errors['maxlength']) {
      errorMessage = '請輸入小於 16 個字'
    } 
    else if (formControl.errors['pattern']) {
      errorMessage = '此欄位只能包含英數字'
    } 
    else if (formControl.errors['email']) {
      errorMessage = '信箱格式錯誤'
    }

    return errorMessage!
  }

  register() {
    const { nickname, username, password, email } = this.registerForm.value

    if (this.registerForm.invalid) {
      console.log('hi')
    } else {
      console.log('hihi')
    }
  }

}
